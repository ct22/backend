Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  scope '/api/v1' do
    resources :users
    resources :events
    resources :todos, only: [:index, :create, :update, :destroy]
    resources :suggestions, only: [:create, :index]

    resources :account_activations, only: [:edit]
    resources :password_resets,     only: [:create, :update]

    get '/admin/users' => "admin#users"



    get 'todos/last_archived', to: 'todos#archived'
    resources :lists do 
      resources :list_items
    end
    post '/auth/login', to: 'authentication#login'
    get '/*a', to: 'application#not_found'
  end  
end
