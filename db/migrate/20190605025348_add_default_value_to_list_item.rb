class AddDefaultValueToListItem < ActiveRecord::Migration[5.2]
  def change
    change_column_default :list_items, :done, from: nil, to: false
  end
end
