class AddDetailsToTodos < ActiveRecord::Migration[5.2]
  def change
    add_column :todos, :due_date, :date
    add_column :todos, :due_time, :time
  end
end
