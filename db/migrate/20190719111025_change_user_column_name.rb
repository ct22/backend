class ChangeUserColumnName < ActiveRecord::Migration[5.2]
  def change
    rename_column :users, :activation_digest, :activation_token
  end
end
