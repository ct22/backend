class AddDefaultValueForEvent < ActiveRecord::Migration[5.2]
  def change
    change_column_default :events, :all_day, from: nil, to: false
    # Event.where(all_day: nil).update_all(all_day: false)
  end
end
