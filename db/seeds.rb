# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Todo.create(title: "Buy food: milk, bread, fruits", done: false)
# Todo.create(title: "Imagine Dragons tickets", done: false)

event1 = Event.create!({
    title: 'Event 1',
    start_time: DateTime.new(2019,5,2,8),
    end_time: DateTime.new(2019,5,2,10),
    user_id: User.first.id
  })

event2 = Event.create!({
    title: 'Event 2',
    start_time: DateTime.new(2019,5,5,12),
    end_time: DateTime.new(2019,5,5,15),
    user_id: User.first.id
  })

p event1
p event2