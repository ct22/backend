# Next steps:

- run a script to generate activation_digests for all users present
  
- hook in remember_me
  
- add 'last_signed_in_at' to users
   
- build a 'reset password' process

- add a 'label' model
  - title
  - color (hex)

- add pipeline model
  - has many:
    - columns
      - have many
        - tasks


or....................... one pipeline with a 'column' property. type.... json.