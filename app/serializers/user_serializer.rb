class UserSerializer
    include FastJsonapi::ObjectSerializer
    attributes :email, :id, :created_at, :admin, :activated, :todos_count, :lists_count, :events_count
    
    attribute :todos_count do |object|
        object.todos.count
    end
    attribute :lists_count do |object|
        object.lists.count
    end
    attribute :events_count do |object|
        object.events.count
    end
  end
  