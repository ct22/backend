class ListSerializer
  include FastJsonapi::ObjectSerializer
  attributes :title, :archived, :user_id, :created_at, :updated_at

  has_many :list_items
end
