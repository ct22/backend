class AdminController < ApplicationController
  before_action :authorize_request, except: :create
  include AdminAuthorizable
  
    # authorize! :read  
    # private
      # Use callbacks to share common setup or constraints between actions.
    def users
      authorize!
      @users = User.all.order(:id)
      render json: UserSerializer.new(@users).serialized_json
      # render json: @users, status: :ok
    end

  end
  