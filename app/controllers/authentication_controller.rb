class AuthenticationController < ApplicationController
    before_action :authorize_request, except: :login
  
    # POST /auth/login
    def login
      @user = User.find_by_email(params[:email])
      if @user&.authenticate(params[:password])
        if params[:remember_me] == true
          time = Time.now + 5.days.to_i
          token = JsonWebToken.encode({user_id: @user.id}, 5.days.from_now)
        else
          time = Time.now + 24.hours.to_i
          token = JsonWebToken.encode(user_id: @user.id)
        end
        render json: { 
          token: token, 
          exp: time.strftime("%m-%d-%Y %H:%M"),
          user: @user.as_json
        }, status: :ok
      else
        render json: { error: 'unauthorized' }, status: :unauthorized
      end
    end
  
    private
  
    def login_params
      params.permit(:email, :password, :remember_me)
    end
  end
