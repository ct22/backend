class AccountActivationsController < ApplicationController

    def edit
        user = User.find_by(email: params[:email])
        if user && !user.activated? && user.token_match(user, params[:id])
            user.update_attribute(:activated, true)
            user.update_attribute(:activated_at, Time.zone.now)
            params = '?became_activated=true'
            redirect_to Rails.env == 'production' ? "#{PROD_FRONTEND_URL}#{params}" : "#{DEV_FRONTEND_URL}#{params}"
        else
            params = '?became_activated=false'
            redirect_to Rails.env == 'production' ? "#{PROD_FRONTEND_URL}#{params}" : "#{DEV_FRONTEND_URL}#{params}"
        end
    end 

end
