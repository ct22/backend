class UsersController < ApplicationController
  before_action :authorize_request, except: :create
  before_action :find_user, except: %i[create index]
  include AdminAuthorizable
  
    # GET /users
    def index
      authorize!
      @users = User.all
      render json: @users, status: :ok
    end
  
    # GET /users/{id}
    def show
      render json: @user, status: :ok
    end
  
    # POST /users
    def create
      @user = User.new(user_params)
      if @user.save
        UserMailer.account_activation(@user).deliver_now
        token = JsonWebToken.encode(user_id: @user.id)
        if params[:remember_me] == true
          time = Time.now + 5.days.to_i
        else
          time = Time.now + 24.hours.to_i
        end

        render json: { 
          token: token, 
          exp: time.strftime("%m-%d-%Y %H:%M"),
          user: @user.as_json
        }, status: :ok
      else
        render json: { errors: @user.errors.full_messages },
               status: :unprocessable_entity
      end
    end
  
    # PUT /users/{id}
    def update
      unless @user.update(user_params)
        render json: { errors: @user.errors.full_messages },
               status: :unprocessable_entity
      end
    end
  
    # DELETE /users/{id}
    def destroy
      @user.destroy
    end
  
    private
  
    def find_user
      @user = User.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        render json: { errors: 'User not found' }, status: :not_found
    end
  
    def user_params
      params.permit( :email, :password, :password_confirmation, :remember_me)
    end
  end