module AdminAuthorizable
   extend ActiveSupport::Concern
   class NotPermittedException < StandardError; end

    included do
        rescue_from NotPermittedException, with: -> { render json: { error: 'Not Permitted' }, status: :forbidden }
    end

    def authorize!
        raise NotPermittedException if !@current_user.admin?
        true
    end
end