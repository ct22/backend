class TodosController < ApplicationController
  before_action :authorize_request
  # before_action :set_todo, only: [:show]
  # before_action :find_user

    def index
        user_todos = Todo.where(user_id: @current_user.id)
        todos = user_todos.where(done: false)
          .or(user_todos.where(done: true).where("updated_at >= ?", Time.zone.now.beginning_of_day))
          .or(user_todos.where("due_date >= ?", Date.today))
          .order("created_at DESC")
        render json: todos
    end

    def archived
      archived = Todo.where(user_id: @current_user.id)
        .where(done: true)
        .where("updated_at <= ? ", Time.zone.now.beginning_of_day)
        .order("updated_at DESC")
        .limit(10)

        render json: archived
    end


      def create
        @todo = Todo.new(todo_param)
        if @todo.save
          render json: @todo
        else
          render json: { errors: @todo.errors.full_messages },
               status: :unprocessable_entity
        end
      end
    
      def update
        todo = Todo.find(params[:id])
        todo.update_attributes(todo_param)
        render json: todo
      end
    
      def destroy
        todo = Todo.find(params[:id])
        todo.destroy
        head :no_content, status: :ok
      end
      
    private
        def set_todo
          @todo = Todo.find(params[:id])
        end

        def todo_param
            params.require(:todo).permit(:title, :done, :due_date, :user_id)
        end

        def find_user
          @user = User.find(params[:id])
          rescue ActiveRecord::RecordNotFound
            render json: { errors: 'User not found' }, status: :not_found
        end
end
