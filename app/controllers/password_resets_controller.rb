class PasswordResetsController < ApplicationController
  before_action :get_user, only: [:update]
  before_action :check_expiration, only: [:update]    # Case (1)

  def create
    @user = User.find_by(email: params[:password_reset][:email].downcase)
    if @user
      @user.create_reset_digest
      @user.send_password_reset_email
      render json: {email: @user.email}, status: :ok
    else
      render json: { error: 'There is no user with this email' }, status: :unauthorized
    end
  end
  
  def update
    if params[:user][:password].empty?
      render json: { error: 'Password is empty' }, status: :unauthorized
    else
      @token = params[:id]
      if @user && @user.authenticated?(:reset, @token)
        @user.update_attributes(user_params)
        @user.update_attribute(:reset_digest, nil)
        @user.update_attribute(:reset_sent_at, nil)
        
        render json: {email: @user.email}, status: :ok
      else 
        render json: { error: 'unauthorized' }, status: :unauthorized
      end

    end

  end

  private 
  
  def user_params
    params.require(:user).permit(:password, :password_confirmation)
  end

  def get_user
    @user = User.find_by(email: params[:password_reset][:email])
  end

  def check_expiration
    if @user.password_reset_expired?
      render json: { error: 'Password reset expired' }, status: :unauthorized
    end
  end

end