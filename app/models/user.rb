class User < ApplicationRecord
    attr_accessor :reset_token
    before_save { self.email = email.downcase }
    before_create :create_activation_token

    has_secure_password
    validates :email, presence: true, uniqueness: true
    validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }
    validates :password,
              presence: true,    
              length: { minimum: 6 },

              if: -> { new_record? || !password.nil? }
    has_many :todos
    has_many :lists
    has_many :suggestions
    has_many :events
              
  def as_json(options={})
    super(:only => [:email, :id, :created_at, :admin, :activated])
  end

  # Creates and assigns the activation token and digest. 
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  def User.new_token
    SecureRandom.urlsafe_base64
  end

  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  # Returns true if the given token matches the digest.
  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    total_bool = BCrypt::Password.new(digest).is_password?(token)
    return total_bool
  end

  def token_match(user, token)
    user.activation_token == token
  end

  # Sets the password reset attributes.
  def create_reset_digest
    self.reset_token = User.new_token
    update_attribute(:reset_digest,  User.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end

  # Sends activation email.
  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end

  # Sends password reset email.
  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end

  def password_reset_expired?
    self.reset_sent_at < 2.hours.ago
  end
  
  private

  def create_activation_token
    self.activation_token  = User.new_token
    # self.activation_digest = User.digest(activation_token)
  end

  def validate_email
    self.activated = true
    self.activation_token = nil
  end


end
