class UserMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.account_activation.subject
  #

  def account_activation(user)
    @user = user
    mail to: user.email, subject: "Account activation"
  end

  def password_reset(user)
    @user = user
    @front_end_link = Rails.env == 'production' ? "#{PROD_FRONTEND_URL}/reset_password/#{user.reset_token}?email=#{user.email}" : "#{DEV_FRONTEND_URL}/reset_password/#{user.reset_token}?email=#{user.email}"
    mail to: user.email, subject: "Password reset"
  end

end
