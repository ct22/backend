class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@planit.best'
  layout 'mailer'
end
